import React, { useState, useEffect } from 'react'
import { Text, View, Button, FlatList,TouchableHighlight  } from 'react-native'
import { globalStyles } from '../styles/global'

export default function home({navigation}) {
  // const pressHandler = () => {
  //   // navigation.navigate('ReviewDetailsRoute');
  //   navigation.push('ReviewDetailsRoute');
  // }
  const url = "https://api.covid19api.com/countries";
  const [countries, setCountries] = useState([])
  useEffect(() => {
    fetch(url)
    .then((res) => res.json())
    .then((resJSON) => {
      return res.countries
    })
    .then( countries => {
      setCountries(countries);
      setLoading(false);
    })
    .catch( error => {
      console.error(error);
    })
  }, []);

  return (
    <View style={globalStyles.container}>
      <FlatList
      data={countries}
      keyExtractor={({ id }, index) => id}
      renderItem={({ item }) => (
        <TouchableHighlight onPress={() => navigation.navigate('ReviewDetailsRoute' ,item)} >
          <Text style={globalStyles.text}>{item.title}</Text>
          <Text>{item.Country}</Text>
        </TouchableHighlight>
      /*<Button title="Go to Reviews screen" onPress={pressHandler} />*//**/
      )}
      />
    </View>
  )
}
