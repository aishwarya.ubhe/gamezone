import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native';
import { globalStyles } from '../styles/global'

export default function reviewDetails({ navigation }) {
  // const pressHandler = () => {
  //   navigation.goBack();
  // }
  return (
    <View style={globalStyles.reviewText}>
      <Text>{navigation.getParam('title')}</Text>
      <Text>{navigation.getParam('rating')}</Text>
      <Text>{navigation.getParam('body')}</Text>
      {/*<Button title="back to home" onPress={pressHandler} />*/}
    </View>
  )
}