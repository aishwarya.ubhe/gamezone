import { createStackNavigator } from 'react-navigation-stack'
import About from '../screens/about'
import Header from '../shared/header'
import React from 'react'

const screens = {
  AboutRoute: {
    screen: About,
    navigationOptions: ({ navigation  }) => {
      return {
        headerTitle: () => <Header navigation={navigation} title="About" />
      }
    }
  },
}

const AboutStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    textAlign: 'center',
    headerStyle: {
      backgroundColor: '#bbb'
    },
    headerTitleStyle: {
        fontWeight: 'bold',
        textAlign:'center'
    },
  }
},
  // { navigationOptions : {
  //   headerTitle: (
  //    <Image source={require('../assets/icon.png')} />
  //    )
  // }}
);

export default AboutStack