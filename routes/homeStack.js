import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import Home from '../screens/home'
import ReviewDetails from '../screens/reviewDetails'
import Header from '../shared/header'


const screens = {
  HomeRoute: {
    screen: Home,
    navigationOptions: ({ navigation,countries }) => {
      return{
        headerTitle: () => <Header navigation ={navigation} countries={countries} title="GameZone"/>
      }
    }
  },
  ReviewDetailsRoute: {
    screen: ReviewDetails,
    navigationOptions: {
     title: 'Review Details',
    }
  },
}

const HomeStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    textAlign: 'center',
    headerTintColor: "#444",
    headerStyle: {
      // backgroundColor: '#bbb'
    },
    headerTitleStyle: {
        fontWeight: 'bold',
        textAlign:'center'
    },
  }
},
  // { navigationOptions : {
  //   headerTitle: (
  //    <Image source={require('../assets/icon.png')} />
  //    )
  // }}
);

export default HomeStack