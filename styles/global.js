import { StyleSheet } from 'react-native'

export const globalStyles = StyleSheet.create({
  container: {
    padding: 20,
  },
  text: {
    fontFamily: 'nunito-bold',
    borderWidth: 1,
    borderColor: '#333',
    padding: 10,
    margin: 10,
  },
  reviewText: {
    borderWidth: 1,
    borderColor: '#333',
    margin:20,
    padding:20,
  },
  paragraph: {
    marginVertical:8,
    lineHeight: 10,
  }
})